const { AuthenticationError } = require("apollo-server");


async function insertPost(parent, args, context, info) {

    if(context.userId === null) {
      throw new AuthenticationError();
      // throw new Error('You must be authenticated to access this entity');
    }

    const post = await context.prisma.post.create({ 
      data: { 
        title: args.title,
        content: args.content,
        published: args.published,
        authorId: context.userId
      } 
    });
  
    const lastFivePosts = context.prisma.post.findMany({take: 5, orderBy: {id: 'desc'} })

    context.pubsub.publish("POST_LIST", lastFivePosts)

    return post
}
  
module.exports = {
    insertPost
}