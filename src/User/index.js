const { typeDef } = require("./TypeDefs");
const Query = require('./resolvers/Query')
const Mutation = require('./resolvers/Mutation')

module.exports = {
  resolvers: {
    Query,
    Mutation
  },
  typeDef
};


