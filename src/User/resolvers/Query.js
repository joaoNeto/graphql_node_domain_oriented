async function users(parent, args, context, info) {
  args.posts = {}

  const postField = info.fieldNodes[0].selectionSet.selections.find(field => field.name.value == 'posts');
  postField?.arguments.forEach(argument => args.posts[argument.name.value] = argument.value.value)

  return context.prisma.user.findMany({
    skip: args.skip || 0, // start index
    take: args.take || 5, // limit data
    orderBy: args.orderBy,
    include: {
      posts: {
        skip: parseInt(args.posts.skip) || 0,
        take: parseInt(args.posts.take) || 5,
        orderBy: {id: 'desc'}
      },
      profile: true
    }
  });
}

module.exports = {
  users
}