const { gql } = require("apollo-server");

const typeDef = gql`
  type Profile {
    id: ID!
    name: String!
  }
`;

module.exports = {
  typeDef
};