const { gql } = require("apollo-server");

const typeDef = gql`
  extend type Query {
    posts(skip: Int, take: Int, orderBy: OrderByPost): [Post!]!
  }

  extend type Mutation {
    insertPost(title: String!, content: String!, published: Boolean!): Post!
  }

  extend type Subscription {
    feed: [Post!]!
  }

  type Post {
    id: ID!
    title: String!
    content: String!
    published: Boolean!
    authorId: Int!
  }

  input OrderByPost {
    id: Sort
    title: Sort
    content: Sort
    published: Sort
    authorId: Sort
  }

`;

module.exports = {
  typeDef
};