const { gql } = require("apollo-server");

const typeDef = gql`
    extend type Query {
        users(skip: Int, take: Int, orderBy: OrderByUser): [User!]!
    }

    extend type Mutation {
        signup(name: String!, email: String!, password: String!): AuthPayload
        login(email: String!, password: String!): AuthPayload
    }

    type User {
        id: ID!
        name: String!
        email: String!
        posts(skip: Int, take: Int): [Post]
        profile: Profile!
    }

    type AuthPayload {
        token: String
        user: User
    }

    input OrderByUser {
        id: Sort
        name: Sort
        email: Sort
    }
`;

module.exports = {
  typeDef
};