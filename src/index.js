const { ApolloServer, AuthenticationError } = require('apollo-server');
const { PrismaClient } = require('@prisma/client')
const { getUserId } = require('./utils');
const { PubSub } = require('apollo-server')
const { gql } = require("apollo-server");

const prisma = new PrismaClient()
const pubsub = new PubSub()

const user = require("./User");
const post = require("./Post");
const profile = require("./Profile");

const typeDef = gql`
  type Query
  type Mutation
  type Subscription

  enum Sort {
    asc
    desc
  }
`;

const server = new ApolloServer({
    typeDefs: [
      typeDef,
      user.typeDef,
      post.typeDef,
      profile.typeDef,
    ],
    resolvers: [
      user.resolvers,
      post.resolvers,
      profile.resolvers,
    ],
    context: ({ req }) => {
      return {
        ...req,
        prisma,
        pubsub,
        userId:
          req && req.headers.authorization
            ? getUserId(req)
            : null
      }
    },
    formatError: (err) => {

      if (err.originalError instanceof AuthenticationError) {
        return 'You must be authenticated to access this entity';
      }

      return err;
    }
})

server
  .listen()
  .then(({ url }) =>
    console.log(`Server is running on ${url}`)
  );