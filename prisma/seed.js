const { PrismaClient } = require('@prisma/client')
const userSeed = require('./seeder/users')
const postSeed = require('./seeder/posts')
const bcrypt = require('bcryptjs')

const prisma = new PrismaClient();

async function main() {

    const profileNames = ['employee', 'manager', 'administrator'];

    for(let user of userSeed) {
        user.password = await bcrypt.hash(user.password, 10);
        const userCreated = await prisma.user.create({ data: user });

        const profileIndex = Math.floor(Math.random() * (profileNames.length));    
        await prisma.profile.create({ data: { name: profileNames[profileIndex], userId: userCreated.id } });

        for(let post of postSeed) {
            post.authorId = userCreated.id;
            await prisma.post.create({ data: post });
        }

    }
}

main().catch(e => {
    console.log(e);
    process.exit(1);
}).finally(() => {
    prisma.$disconnect()
})