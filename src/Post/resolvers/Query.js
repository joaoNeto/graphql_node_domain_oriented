function posts(parent, args, context, info) {
    return context.prisma.post.findMany({
        skip: args.skip || 0, // start index
        take: args.take || 5, // limit data
        orderBy: args.orderBy,    
    })
}

module.exports = {
    posts
}