const { typeDef } = require("./TypeDefs");
const Query = require('./resolvers/Query')
const Mutation = require('./resolvers/Mutation')
const Subscription = require('./resolvers/Subscription')

module.exports = {
  resolvers: {
    Query,
    Mutation,
    Subscription
  },
  typeDef
};


