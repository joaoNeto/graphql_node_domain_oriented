# GraphQL + Node.js + prisma

A simple [GraphQL](https://graphql.org/) project using [Node.js](https://nodejs.org/en/) and [prisma](https://www.prisma.io/) database 

## Instalation

To install and run it, you just need download all of the dependencies and migrate the database, so download the project and execute the command below in your terminal, make sure you have the [node on your machine](https://nodejs.org/en/download/) and after that remember to be in the project directory to execute the commands below.

`$ npm install`

The command above downloaded all of the dependencies of the project, 

`$ npx prisma migrate dev --name "init" && npx prisma generate`

The command above created and structured your database and that's it :) now you can run the project.

## Running

After installed to run the project it's much more simple, we are using nodemon because it's easier
 when you are codding in the project, if you don't have the nodemon, please execute the command `$ npm install -g nodemon`, after installed nodemon please execute the command below:
 
 
`$ nodemon src/index.js`



## Command utils

`$ npx prisma studio`

`$ npx prisma db seed --preview-feature`


## Articles

* https://hackernoon.com/three-ways-to-structure-your-graphql-code-with-apollo-server-4788beed89db
* https://www.howtographql.com/
* https://graphql.org/learn/authorization/
* https://www.graph.cool/
* https://graphql.org/learn/schema/#type-system
* https://www.apollographql.com/docs/apollo-server/data/errors/