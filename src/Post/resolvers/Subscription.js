function listPostSubscribe(parent, args, context, info) {
    return context.pubsub.asyncIterator("POST_LIST")
}

const feed = {
    subscribe: listPostSubscribe,
    resolve: payload => {
        return payload
    },
}

module.exports = {
    feed
}